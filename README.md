Adding and rotating keys
------------------------

Preferablly load archive signer keys into an ephemeral GnuPG homdir,
strip off everything but the signing keys, and then export thusly:

for user in user1 user2
do
  gpg --armor --export-options export-minimal --export "$user" > $user".asc
done
